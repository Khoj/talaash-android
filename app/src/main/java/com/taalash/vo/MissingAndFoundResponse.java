package com.taalash.vo;

/**
 * Created by imittal on 5/16/16.
 */
public class MissingAndFoundResponse {

    private String name;
    private String age;
    private String sex;
    private String identificationMarkOne;
    private String identificationMarkTwo;
    private String lastLocatedAt;
    private String imagePath;
    private int caseId;


    @Override
    public String toString() {
        return "MissingAndFoundResponse{" +
                "name='" + name + '\'' +
                ", age='" + age + '\'' +
                ", sex='" + sex + '\'' +
                ", identificationMarkOne='" + identificationMarkOne + '\'' +
                ", identificationMarkTwo='" + identificationMarkTwo + '\'' +
                ", lastLocatedAt='" + lastLocatedAt + '\'' +
                ", imagePath='" + imagePath + '\'' +
                '}';
    }


    public int getCaseId() {
        return caseId;
    }

    public void setCaseId(int caseId) {
        this.caseId = caseId;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getIdentificationMarkOne() {
        return identificationMarkOne;
    }

    public void setIdentificationMarkOne(String identificationMarkOne) {
        this.identificationMarkOne = identificationMarkOne;
    }

    public String getIdentificationMarkTwo() {
        return identificationMarkTwo;
    }

    public void setIdentificationMarkTwo(String identificationMarkTwo) {
        this.identificationMarkTwo = identificationMarkTwo;
    }

    public String getLastLocatedAt() {
        return lastLocatedAt;
    }

    public void setLastLocatedAt(String lastLocatedAt) {
        this.lastLocatedAt = lastLocatedAt;
    }

}
