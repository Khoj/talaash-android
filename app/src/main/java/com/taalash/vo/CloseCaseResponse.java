package com.taalash.vo;

/**
 * Created by imittal on 5/20/16.
 */
public class CloseCaseResponse {
    private boolean error;

    public CloseCaseResponse(boolean error) {
        this.error = error;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }
}
