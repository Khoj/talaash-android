package com.taalash.vo;

/**
 * Created by imittal on 5/12/16.
 */
public class LogInResponse {
    private String userToken;
    private boolean error;

    public LogInResponse(String userToken, boolean error) {
        this.userToken = userToken;
        this.error = error;
    }

    @Override
    public String toString() {
        return "LogInResponse{" +
                "userToken='" + userToken + '\'' +
                ", error=" + error +
                '}';
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }
}
