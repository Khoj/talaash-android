package com.taalash.vo;

/**
 * Created by imittal on 5/18/16.
 */
public class MissingCaseResponse {
    private boolean error;
    private String caseId;

    public MissingCaseResponse(boolean error) {
        this.error = error;
    }

    public String getCaseId() {
        return caseId;
    }

    public void setCaseId(String caseId) {
        this.caseId = caseId;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }
}
