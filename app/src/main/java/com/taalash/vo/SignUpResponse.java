package com.taalash.vo;

/**
 * Created by imittal on 5/15/16.
 */
public class SignUpResponse {
    private String userToken;
    private boolean error;

    public boolean getError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }
}
