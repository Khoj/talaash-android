package com.taalash.vo;

/**
 * Created by imittal on 5/17/16.
 */
public class GenerateOtpResponse {
    private String otp;
    private boolean error;

    public GenerateOtpResponse(String otp, boolean error) {
        this.otp = otp;
        this.error = error;
    }

    @Override
    public String toString() {
        return "GenerateOtpResponse{" +
                "otp='" + otp + '\'' +
                ", error=" + error +
                '}';
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }
}
