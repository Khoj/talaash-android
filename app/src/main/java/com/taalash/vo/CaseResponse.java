package com.taalash.vo;

import com.taalash.model.Status;

import java.util.List;

/**
 * Created by imittal on 5/14/16.
 */
public class CaseResponse {

    private String caseId;
    private String name;
    private String age;
    private String sex;
    private String height;
    private String heightUnit;
    private String identificationMarkOne;
    private String identificationMarkTwo;
    private List<Status> statuses;
    private List<String> imagePaths;

    @Override
    public String toString() {
        return "MyCaseResponse{" +
                "caseId='" + caseId + '\'' +
                ", name='" + name + '\'' +
                ", age='" + age + '\'' +
                ", sex='" + sex + '\'' +
                ", height='" + height + '\'' +
                ", identificationMarkOne='" + identificationMarkOne + '\'' +
                ", identificationMarkTwo='" + identificationMarkTwo + '\'' +
                ", statuses=" + statuses +
                ", imagePaths=" + imagePaths +
                '}';
    }

    public String getHeightUnit() {
        return heightUnit;
    }

    public void setHeightUnit(String heightUnit) {
        this.heightUnit = heightUnit;
    }

    public List<String> getImagePaths() {
        return imagePaths;
    }

    public void setImagePaths(List<String> imagePaths) {
        this.imagePaths = imagePaths;
    }

    public List<Status> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<Status> statuses) {
        this.statuses = statuses;
    }

    public String getCaseId() {
        return caseId;
    }

    public void setCaseId(String caseId) {
        this.caseId = caseId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getIdentificationMarkOne() {
        return identificationMarkOne;
    }

    public void setIdentificationMarkOne(String identificationMarkOne) {
        this.identificationMarkOne = identificationMarkOne;
    }

    public String getIdentificationMarkTwo() {
        return identificationMarkTwo;
    }

    public void setIdentificationMarkTwo(String identificationMarkTwo) {
        this.identificationMarkTwo = identificationMarkTwo;
    }
}
