package com.taalash.vo;

/**
 * Created by imittal on 5/22/16.
 */
public class ReportFoundResponse {
    private boolean error;

    public ReportFoundResponse(boolean error) {
        this.error = error;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }
}
