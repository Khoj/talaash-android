package com.taalash.model;

/**
 * Created by imittal on 5/15/16.
 */
public class MissingCase {
    private String name;
    private String age;
    private String height;
    private String heightUnit;
    private String identificationMarkOne;
    private String identificationMarkTwo;
    private String gender;
    private String languageOne;
    private String languageTwo;
    private String languageThree;
    private String languageFour;
    private String lastLocatedAt;
    private String relationToMissingPerson;
    private String details;
    private String otherDetails;
    private String date;
    private String time;
    private int numberOfPics;
    private Boolean statusOfCase;

    public int getNumberOfPics() {
        return numberOfPics;
    }

    public void setNumberOfPics(int numberOfPics) {
        this.numberOfPics = numberOfPics;
    }

    public Boolean getStatusOfCase() {
        return statusOfCase;
    }

    public void setStatusOfCase(Boolean statusOfCase) {
        this.statusOfCase = statusOfCase;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getHeightUnit() {
        return heightUnit;
    }

    public void setHeightUnit(String heightUnit) {
        this.heightUnit = heightUnit;
    }

    public String getIdentificationMarkOne() {
        return identificationMarkOne;
    }

    public void setIdentificationMarkOne(String identificationMarkOne) {
        this.identificationMarkOne = identificationMarkOne;
    }

    public String getIdentificationMarkTwo() {
        return identificationMarkTwo;
    }

    public void setIdentificationMarkTwo(String identificationMarkTwo) {
        this.identificationMarkTwo = identificationMarkTwo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLanguageOne() {
        return languageOne;
    }

    public void setLanguageOne(String languageOne) {
        this.languageOne = languageOne;
    }

    public String getLanguageTwo() {
        return languageTwo;
    }

    public void setLanguageTwo(String languageTwo) {
        this.languageTwo = languageTwo;
    }

    public String getLanguageThree() {
        return languageThree;
    }

    public void setLanguageThree(String languageThree) {
        this.languageThree = languageThree;
    }

    public String getLanguageFour() {
        return languageFour;
    }

    public void setLanguageFour(String languageFour) {
        this.languageFour = languageFour;
    }

    public String getLastLocatedAt() {
        return lastLocatedAt;
    }

    public void setLastLocatedAt(String lastLocatedAt) {
        this.lastLocatedAt = lastLocatedAt;
    }

    public String getRelationToMissingPerson() {
        return relationToMissingPerson;
    }

    public void setRelationToMissingPerson(String relationToMissingPerson) {
        this.relationToMissingPerson = relationToMissingPerson;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getOtherDetails() {
        return otherDetails;
    }

    public void setOtherDetails(String otherDetails) {
        this.otherDetails = otherDetails;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
