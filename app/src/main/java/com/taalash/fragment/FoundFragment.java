package com.taalash.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.taalash.R;
import com.taalash.adapter.MissingAndFoundPeopleAdapter;
import com.taalash.util.GSONRequest;
import com.taalash.util.SessionManager;
import com.taalash.util.VolleyRequestQueue;
import com.taalash.vo.MissingAndFoundResponse;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FoundFragment extends Fragment {

    private RecyclerView foundList;
    private View rootView;
    private List<MissingAndFoundResponse> foundPeople;
    private MissingAndFoundPeopleAdapter adapter;
    private SessionManager sessionManager;

    public static Fragment newInstance() {
        Fragment fragment = new FoundFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_found, container, false);
        initializeViews();
        sessionManager = new SessionManager(getActivity().getApplicationContext());
        foundList.setHasFixedSize(true);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        foundList.setLayoutManager(llm);

        getFoundList();


        return rootView;
    }

    private void getFoundList() {

        Map<String, String> headers = new HashMap<>();
        headers.put("User-Token", sessionManager.getUserToken());


        GSONRequest<MissingAndFoundResponse[]> getFoundPeople = new GSONRequest<MissingAndFoundResponse[]>(Request.Method.POST, getResources().getString(R.string.get_all_found), MissingAndFoundResponse[].class, headers, new Response.Listener<MissingAndFoundResponse[]>() {
            @Override
            public void onResponse(MissingAndFoundResponse[] response) {
                foundPeople = Arrays.asList(response);
                adapter = new MissingAndFoundPeopleAdapter(foundPeople, getActivity());
                foundList.setAdapter(adapter);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("error:foundPeople", error.toString());
            }
        });
        VolleyRequestQueue.getInstance(getActivity()).addToRequestQueue(getFoundPeople);

    }

    private void initializeViews() {
        foundList = (RecyclerView) rootView.findViewById(R.id.found_list);
    }

}
