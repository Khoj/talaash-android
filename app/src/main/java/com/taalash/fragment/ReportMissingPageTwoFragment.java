package com.taalash.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.taalash.R;
import com.taalash.communicators.OnSubmitClickedListener;
import com.taalash.util.DatePickerFragment;
import com.taalash.util.SpinnerList;
import com.taalash.util.TimePickerFragment;

/**
 * Created by imittal on 5/10/16.
 */
public class ReportMissingPageTwoFragment extends Fragment {
    private View rootView;
    private EditText languageOne, languageTwo, languageThree, languageFour, lastLocatedAt, relationToMissingAt, otherDetails;
    private Spinner details;
    private Button date, time, submit;
    private OnSubmitClickedListener mCallback;
    private ArrayAdapter<String> detailsAdapter;

    public static Fragment newInstance() {
        Fragment fragment = new ReportMissingPageTwoFragment();
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (OnSubmitClickedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnSubmitClickedListener");
        }
    }


    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_report_missing_page_two, container, false);
        initializeViews();

        setTime();
        setDate();

        detailsAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, SpinnerList.getDetailsList());
        details.setAdapter(detailsAdapter);


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onSubmitClicked(languageOne.getText().toString(), languageTwo.getText().toString(),
                        languageThree.getText().toString(), languageFour.getText().toString(), lastLocatedAt.getText().toString(),
                        relationToMissingAt.getText().toString(), otherDetails.getText().toString(),
                        details.getSelectedItem().toString(), date.getText().toString(), time.getText().toString());
            }
        });

        return rootView;
    }

    private void setDate() {
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog(v);
            }
        });
    }

    private void setTime() {
        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePickerDialog(v);
            }
        });
    }

    private void initializeViews() {
        languageOne = (EditText) rootView.findViewById(R.id.point_one_value);
        languageTwo = (EditText) rootView.findViewById(R.id.point_two_value);
        languageThree = (EditText) rootView.findViewById(R.id.point_three_value);
        languageFour = (EditText) rootView.findViewById(R.id.point_four_value);
        lastLocatedAt = (EditText) rootView.findViewById(R.id.last_located_at_value);
        relationToMissingAt = (EditText) rootView.findViewById(R.id.relation_to_missing_person_value);
        otherDetails = (EditText) rootView.findViewById(R.id.other_details_value);
        details = (Spinner) rootView.findViewById(R.id.details_spinner);
        date = (Button) rootView.findViewById(R.id.set_date_button);
        time = (Button) rootView.findViewById(R.id.set_time_button);
        submit = (Button) rootView.findViewById(R.id.submit_button);
    }

    public void showTimePickerDialog(View v) {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getActivity().getSupportFragmentManager(), "timePicker");
    }

    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getActivity().getSupportFragmentManager(), "datePicker");
    }


}
