package com.taalash.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.taalash.R;
import com.taalash.adapter.MissingAndFoundPeopleAdapter;
import com.taalash.util.GSONRequest;
import com.taalash.util.SessionManager;
import com.taalash.util.VolleyRequestQueue;
import com.taalash.vo.MissingAndFoundResponse;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MissingFragment extends Fragment {

    private RecyclerView missingList;
    private View rootView;
    private List<MissingAndFoundResponse> missingPeople;
    private MissingAndFoundPeopleAdapter adapter;
    private SessionManager sessionManager;

    public static Fragment newInstance() {
        Fragment fragment = new MissingFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_missing, container, false);
        sessionManager = new SessionManager(getActivity().getApplicationContext());
        initializeViews();
        missingList.setHasFixedSize(true);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        missingList.setLayoutManager(llm);

        getFoundList();

        return rootView;
    }

    private void getFoundList() {

        Map<String, String> headers = new HashMap<>();
        headers.put("User-Token", sessionManager.getUserToken());

        GSONRequest<MissingAndFoundResponse[]> getMissingPeople = new GSONRequest<>(Request.Method.POST, getResources().getString(R.string.get_all_missing), MissingAndFoundResponse[].class, headers, new Response.Listener<MissingAndFoundResponse[]>() {
            @Override
            public void onResponse(MissingAndFoundResponse[] response) {
                missingPeople = Arrays.asList(response);
                adapter = new MissingAndFoundPeopleAdapter(missingPeople, getActivity());
                missingList.setAdapter(adapter);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("error:missingPeople", error.toString());
            }
        }
        );
        VolleyRequestQueue.getInstance(getActivity()).addToRequestQueue(getMissingPeople);

    }

    private void initializeViews() {
        missingList = (RecyclerView) rootView.findViewById(R.id.missing_list);
    }


}
