package com.taalash.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.taalash.R;
import com.taalash.communicators.OnNextClickedListener;
import com.taalash.util.ImageManipulator;
import com.taalash.util.SpinnerList;
import com.taalash.util.UrlBuilder;

import java.io.IOException;

/**
 * Created by imittal on 5/10/16.
 */
public class ReportMissingPageOneFragment extends Fragment {

    private int PICK_IMAGE_REQUEST = 1;
    private EditText name, age, height, visibleIdentificationMarkOne, visibleIdentificationMarkTwo;
    private Spinner gender, heightUnit;
    private Button uploadImage, next;
    private ImageView uploadedImage;
    private OnNextClickedListener mCallback;
    private View rootView;
    private ArrayAdapter<String> genderAdapter, heightAdapter;
    private Uri uri;
    private String encodedImageString;


    public static Fragment newInstance() {
        return new ReportMissingPageOneFragment();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (OnNextClickedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnNextClickedListener");
        }
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_report_missing_page_one, container, false);

        initializeViews();

        genderAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, SpinnerList.getGenderList());
        gender.setAdapter(genderAdapter);

        heightAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, SpinnerList.getHeightUnitsList());
        heightUnit.setAdapter(heightAdapter);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onNextClicked(name.getText().toString(), age.getText().toString(), height.getText().toString(),
                        heightUnit.getSelectedItem().toString(), visibleIdentificationMarkOne.getText().toString(),
                        visibleIdentificationMarkTwo.getText().toString(), gender.getSelectedItem().toString(), encodedImageString);
            }
        });

        uploadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
            }
        });

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.getData() != null) {
            uri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);

                uploadImage.setVisibility(View.GONE);
                uploadedImage.setVisibility(View.VISIBLE);
                bitmap = ImageManipulator.getResizedBitmap(bitmap, 200);// 400 is for example, replace with desired size

                uploadedImage.setImageBitmap(bitmap);
                encodedImageString = UrlBuilder.getStringImage(bitmap);


            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private void initializeViews() {
        name = (EditText) rootView.findViewById(R.id.name_value);
        age = (EditText) rootView.findViewById(R.id.age_value);
        height = (EditText) rootView.findViewById(R.id.height_value);
        visibleIdentificationMarkOne = (EditText) rootView.findViewById(R.id.point_one_value);
        visibleIdentificationMarkTwo = (EditText) rootView.findViewById(R.id.point_two_value);
        gender = (Spinner) rootView.findViewById(R.id.gender_value);
        heightUnit = (Spinner) rootView.findViewById(R.id.height_unit);
        uploadImage = (Button) rootView.findViewById(R.id.upload_image);
        next = (Button) rootView.findViewById(R.id.next);
        uploadedImage = (ImageView) rootView.findViewById(R.id.uploaded_image);
    }


}
