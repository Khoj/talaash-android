package com.taalash.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by imittal on 5/20/16.
 */
public class SpinnerList {

    public static List<String> getGenderList() {
        List<String> genderList = new ArrayList<>();
        genderList.add("F");
        genderList.add("M");
        return genderList;
    }

    public static List<String> getHeightUnitsList() {
        List<String> heightUnitList = new ArrayList<>();
        heightUnitList.add("cm");
        heightUnitList.add("m");
        heightUnitList.add("inch");
        return heightUnitList;
    }

    public static List<String> getDetailsList() {
        List<String> detailsList = new ArrayList<>();
        detailsList.add("Lost while playing in Park");
        detailsList.add("Didn't return from office or school");
        detailsList.add("Others");
        return detailsList;
    }
}
