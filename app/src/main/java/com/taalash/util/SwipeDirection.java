package com.taalash.util;

/**
 * Created by imittal on 5/29/16.
 */
public enum SwipeDirection {
    all, left, right, none
}
