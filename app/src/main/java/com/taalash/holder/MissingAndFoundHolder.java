package com.taalash.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.taalash.R;

/**
 * Created by imittal on 5/16/16.
 */
public class MissingAndFoundHolder extends RecyclerView.ViewHolder {

    public TextView name, age, identificationMarkOne, identificationMarkTwo, lastLocatedAt;
    public NetworkImageView imageView;

    public MissingAndFoundHolder(View v) {
        super(v);

        name = (TextView) v.findViewById(R.id.name_of_missing_person);
        age = (TextView) v.findViewById(R.id.age_and_gender_of_missing_person);
        identificationMarkOne = (TextView) v.findViewById(R.id.identification_mark_one);
        identificationMarkTwo = (TextView) v.findViewById(R.id.identification_mark_two);
        lastLocatedAt = (TextView) v.findViewById(R.id.last_located_at_place);
        imageView = (NetworkImageView) v.findViewById(R.id.photo_of_missing_person);

    }

}
