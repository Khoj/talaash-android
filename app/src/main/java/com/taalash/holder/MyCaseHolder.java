package com.taalash.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.taalash.R;

/**
 * Created by imittal on 5/15/16.
 */
public class MyCaseHolder extends RecyclerView.ViewHolder {

    public TextView place;
    public TextView time;
    public TextView reportedBy;

    public MyCaseHolder(View itemView) {
        super(itemView);
        place = (TextView) itemView.findViewById(R.id.located_at_value);
        time = (TextView) itemView.findViewById(R.id.time_value);
        reportedBy = (TextView) itemView.findViewById(R.id.reporting_person_name);
    }
}
