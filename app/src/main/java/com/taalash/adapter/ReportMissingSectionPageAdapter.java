package com.taalash.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.taalash.fragment.ReportMissingPageOneFragment;
import com.taalash.fragment.ReportMissingPageTwoFragment;

/**
 * Created by imittal on 5/14/16.
 */
public class ReportMissingSectionPageAdapter extends FragmentPagerAdapter {

    public ReportMissingSectionPageAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return ReportMissingPageOneFragment.newInstance();
            case 1:
                return ReportMissingPageTwoFragment.newInstance();

            default:
                return null;

        }
    }

    @Override
    public int getCount() {
        return 2;
    }

}