package com.taalash.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.taalash.R;
import com.taalash.activity.CaseViewActivity;
import com.taalash.holder.MissingAndFoundHolder;
import com.taalash.util.VolleyRequestQueue;
import com.taalash.vo.MissingAndFoundResponse;

import java.util.List;

/**
 * Created by imittal on 5/15/16.
 */
public class MissingAndFoundPeopleAdapter extends RecyclerView.Adapter<MissingAndFoundHolder> {

    private List<MissingAndFoundResponse> listOfMissingAndFoundPeople;
    private Activity activity;
    private Intent viewCaseDetails;

    public MissingAndFoundPeopleAdapter(List<MissingAndFoundResponse> listOfMissingAndFoundPeople, Activity activity) {
        this.listOfMissingAndFoundPeople = listOfMissingAndFoundPeople;
        this.activity = activity;
    }

    @Override
    public MissingAndFoundHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        final View v = layoutInflater.inflate(R.layout.row_missing_and_found, parent, false);
        return new MissingAndFoundHolder(v);
    }

    @Override
    public void onBindViewHolder(MissingAndFoundHolder holder, int position) {
        Log.i("h", listOfMissingAndFoundPeople.get(position).toString());
        viewCaseDetails = new Intent(activity, CaseViewActivity.class);
        viewCaseDetails.putExtra("caseId", listOfMissingAndFoundPeople.get(position).getCaseId());
        holder.name.setText(listOfMissingAndFoundPeople.get(position).getName());
        holder.age.setText(listOfMissingAndFoundPeople.get(position).getAge().concat(",").concat(listOfMissingAndFoundPeople.get(position).getSex()));
        holder.identificationMarkOne.setText(listOfMissingAndFoundPeople.get(position).getIdentificationMarkOne());
        holder.identificationMarkTwo.setText(listOfMissingAndFoundPeople.get(position).getIdentificationMarkTwo());
        holder.lastLocatedAt.setText(listOfMissingAndFoundPeople.get(position).getLastLocatedAt());
        holder.imageView.setImageUrl(listOfMissingAndFoundPeople.get(position).getImagePath(), VolleyRequestQueue.getInstance(activity.getApplication()).getImageLoader());
    }

    @Override
    public int getItemCount() {
        return listOfMissingAndFoundPeople.size();
    }
}
