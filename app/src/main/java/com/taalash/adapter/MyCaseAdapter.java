package com.taalash.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.taalash.R;
import com.taalash.holder.MyCaseHolder;
import com.taalash.model.Status;

import java.util.List;

/**
 * Created by imittal on 5/15/16.
 */
public class MyCaseAdapter extends RecyclerView.Adapter<MyCaseHolder> {

    private List<Status> statuses;

    public MyCaseAdapter(List<Status> statuses) {
        this.statuses = statuses;
    }

    @Override
    public MyCaseHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        final View v = layoutInflater.inflate(R.layout.row_status, parent, false);
        return new MyCaseHolder(v);
    }

    @Override
    public void onBindViewHolder(MyCaseHolder holder, int position) {
        if (statuses != null && statuses.get(position) != null) {
            holder.place.setText(statuses.get(position).getPlace());
            holder.time.setText(statuses.get(position).getTime().concat(",").concat(statuses.get(position).getDate()));
            holder.reportedBy.setText(statuses.get(position).getReportedBy());
        }
    }

    @Override
    public int getItemCount() {
        if (statuses != null) {
            return statuses.size();
        } else
            return 0;
    }
}
