package com.taalash.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.taalash.fragment.FoundFragment;
import com.taalash.fragment.MissingFragment;

/**
 * Created by imittal on 5/12/16.
 */
public class HomeSectionsPagerAdapter extends FragmentPagerAdapter {

    public HomeSectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return MissingFragment.newInstance();
            case 1:
                return FoundFragment.newInstance();

            default:
                return null;

        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Missing";
            case 1:
                return "Found";

        }
        return null;
    }
}
