package com.taalash.communicators;

/**
 * Created by imittal on 5/15/16.
 */
public interface OnNextClickedListener {
    void onNextClicked(String name, String age, String height, String heightUnit,
                       String identificationMarkOne, String identificationMarkTwo,
                       String gender, String encodedString);
}
