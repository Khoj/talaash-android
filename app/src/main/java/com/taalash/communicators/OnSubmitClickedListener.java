package com.taalash.communicators;

/**
 * Created by imittal on 5/15/16.
 */
public interface OnSubmitClickedListener {
    void onSubmitClicked(String languageOne, String languageTwo, String languageThree, String languageFour,
                         String lastLocatedAt, String relationToMissingPerson, String otherDetails, String details,
                         String date, String time);
}
