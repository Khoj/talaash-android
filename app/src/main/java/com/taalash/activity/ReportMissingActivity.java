package com.taalash.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.taalash.R;
import com.taalash.adapter.ReportMissingSectionPageAdapter;
import com.taalash.communicators.OnNextClickedListener;
import com.taalash.communicators.OnSubmitClickedListener;
import com.taalash.model.MissingCase;
import com.taalash.util.GSONRequest;
import com.taalash.util.MyViewPager;
import com.taalash.util.SessionManager;
import com.taalash.util.SwipeDirection;
import com.taalash.util.VolleyRequestQueue;
import com.taalash.vo.MissingCaseResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ReportMissingActivity extends BaseActivity implements OnNextClickedListener, OnSubmitClickedListener {

    private ReportMissingSectionPageAdapter reportMissingSectionPagerAdapter;
    private MyViewPager mViewPager;
    private MissingCase missingCase;
    private String encodedString;
    private SessionManager sessionManager;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.frame_report_missing, frameLayout);
        missingCase = new MissingCase();
        sessionManager = new SessionManager(getApplicationContext());
        reportMissingSectionPagerAdapter = new ReportMissingSectionPageAdapter(getSupportFragmentManager());
        mViewPager = (MyViewPager) findViewById(R.id.create_new_case_view_pager);
        mViewPager.setAdapter(reportMissingSectionPagerAdapter);
        mViewPager.setAllowedSwipeDirection(SwipeDirection.left);

    }


    @Override
    public void onNextClicked(String name, String age, String height,
                              String heightUnit, String identificationMarkOne,
                              String identificationMarkTwo, String gender, String encodedString) {

        this.encodedString = encodedString;
        setNewCaseValuesFromFragmentOne(name, age, height, heightUnit, identificationMarkOne,
                identificationMarkTwo, gender);
        mViewPager.setCurrentItem(1, true);

    }

    private void setNewCaseValuesFromFragmentOne(String name, String age, String height, String heightUnit,
                                                 String identificationMarkOne, String identificationMarkTwo,
                                                 String gender) {
        missingCase.setName(name);
        missingCase.setAge(age);
        missingCase.setHeight(height);
        missingCase.setHeightUnit(heightUnit);
        missingCase.setIdentificationMarkOne(identificationMarkOne);
        missingCase.setIdentificationMarkTwo(identificationMarkTwo);
        missingCase.setGender(gender);
        missingCase.setStatusOfCase(false);
        missingCase.setNumberOfPics(1);
    }

    @Override
    public void onSubmitClicked(String languageOne, String languageTwo, String languageThree, String languageFour,
                                String lastLocatedAt, String relationToMissingPerson, String otherDetails,
                                String details, String date, String time) {
        setNewCaseValuesFromFragmentTwo(languageOne, languageTwo, languageThree,
                languageFour, lastLocatedAt, relationToMissingPerson,
                otherDetails, details, date, time);


        if (encodedString != null) {
            dialog = new ProgressDialog(ReportMissingActivity.this);
            dialog.setMessage("Creating your case");
            dialog.show();
            createNewCase();
        } else {
            Toast.makeText(this, "Please choose an image to upload", Toast.LENGTH_LONG);
        }

    }


    private void createNewCase() {
        JSONObject parameters = new JSONObject();
        try {
            System.out.println("hi");
            // System.out.println("encoded string" + encodedString);
            // parameters.put("missingCase",missingCase);
            parameters.put("imageEncodedString", encodedString);
            parameters.put("name", missingCase.getName());
            parameters.put("age", missingCase.getAge());
            parameters.put("height", missingCase.getHeight());
            parameters.put("heightUnit", missingCase.getHeightUnit());
            parameters.put("identificationMarkOne", missingCase.getIdentificationMarkOne());
            parameters.put("identificationMarkTwo", missingCase.getIdentificationMarkTwo());
            parameters.put("gender", missingCase.getGender());
            parameters.put("languageOne", missingCase.getLanguageOne());
            parameters.put("languageTwo", missingCase.getLanguageTwo());
            parameters.put("languageThree", missingCase.getLanguageThree());
            parameters.put("languageFour", missingCase.getLanguageFour());
            parameters.put("lastLocatedAt", missingCase.getLastLocatedAt());
            parameters.put("relationToMissingPerson", missingCase.getRelationToMissingPerson());
            parameters.put("details", missingCase.getDetails());
            parameters.put("otherDetails", missingCase.getOtherDetails());
            parameters.put("date", missingCase.getDate());
            parameters.put("time", missingCase.getTime());
            parameters.put("noOfImages", missingCase.getNumberOfPics());
            parameters.put("status", missingCase.getStatusOfCase());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put("User-Token", sessionManager.getUserToken());
        // headers.put("Encoded-String", encodedString);

        GSONRequest<MissingCaseResponse> createNewCase = new GSONRequest<>(Request.Method.POST, getResources().getString(R.string.create_new_case), MissingCaseResponse.class, headers, new Response.Listener<MissingCaseResponse>() {
            @Override
            public void onResponse(MissingCaseResponse response) {
                System.out.println(response.toString());
                if (response.isError()) {
                    dialog.dismiss();
                    Toast.makeText(ReportMissingActivity.this, "Case not created", Toast.LENGTH_SHORT).show();

                } else {
                    dialog.dismiss();
                    Toast.makeText(ReportMissingActivity.this, "Case created successfully", Toast.LENGTH_SHORT).show();
                    createCaseCreatedAlertBox(response.getCaseId());

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Log.d("error - caseCreation", error.toString());
                Toast.makeText(ReportMissingActivity.this, "Unable to reach our servers", Toast.LENGTH_SHORT).show();
            }
        }, parameters);
        VolleyRequestQueue.getInstance(ReportMissingActivity.this).addToRequestQueue(createNewCase);
    }

    private void createCaseCreatedAlertBox(String caseNumber) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ReportMissingActivity.this);
        builder.setMessage("Your case has been created with Case Id : " + caseNumber)
                .setTitle("Case Created")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(ReportMissingActivity.this, BaseActivity.class));
                    }
                });


        AlertDialog dialog = builder.create();
        dialog.show();

    }

    private void setNewCaseValuesFromFragmentTwo(String languageOne, String languageTwo, String languageThree,
                                                 String languageFour, String lastLocatedAt, String relationToMissingPerson,
                                                 String otherDetails, String details, String date, String time) {
        missingCase.setLanguageOne(languageOne);
        missingCase.setLanguageTwo(languageTwo);
        missingCase.setLanguageThree(languageThree);
        missingCase.setLanguageFour(languageFour);
        missingCase.setLastLocatedAt(lastLocatedAt);
        missingCase.setRelationToMissingPerson(relationToMissingPerson);
        missingCase.setDetails(details);
        missingCase.setOtherDetails(otherDetails);
        missingCase.setDate(date);
        missingCase.setTime(time);
    }


}
