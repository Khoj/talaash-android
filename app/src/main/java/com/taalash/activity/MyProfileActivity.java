package com.taalash.activity;

import android.os.Bundle;

import com.taalash.R;

public class MyProfileActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.frame_my_profile, frameLayout);
    }

}
