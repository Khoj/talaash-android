package com.taalash.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.taalash.R;
import com.taalash.util.DatePickerFragment;
import com.taalash.util.GSONRequest;
import com.taalash.util.ImageManipulator;
import com.taalash.util.SessionManager;
import com.taalash.util.SpinnerList;
import com.taalash.util.TimePickerFragment;
import com.taalash.util.UrlBuilder;
import com.taalash.util.VolleyRequestQueue;
import com.taalash.vo.ReportFoundResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ReportFoundActivity extends BaseActivity {

    private int PICK_IMAGE_REQUEST = 1;
    private Button uploadPics, submit, date, time;
    private ImageView uploadedImage;
    private EditText approxAge, approxHeight, otherDetails, visibleIdentificationMarkOne, visibleIdentificationMarkTwo, name, lastLocatedAt;
    private Spinner heightUnit, gender;
    private ArrayAdapter<String> heightUnitAdapter, genderAdapter;
    private String encodedImageString;
    private SessionManager sessionManager;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.frame_report_found, frameLayout);
        sessionManager = new SessionManager(getApplicationContext());
        initializeViews();
        setTime();
        setDate();

        heightUnitAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, SpinnerList.getHeightUnitsList());
        heightUnit.setAdapter(heightUnitAdapter);
        genderAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, SpinnerList.getGenderList());
        gender.setAdapter(genderAdapter);


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new ProgressDialog(ReportFoundActivity.this);
                dialog.show();
                reportFound();
            }
        });

        uploadPics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
            }
        });


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.getData() != null) {
            Uri uri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                uploadPics.setVisibility(View.GONE);
                uploadedImage.setVisibility(View.VISIBLE);
                bitmap = ImageManipulator.getResizedBitmap(bitmap, 200);// 400 is for example, replace with desired size
                uploadedImage.setImageBitmap(bitmap);
                encodedImageString = UrlBuilder.getStringImage(bitmap);


            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private void reportFound() {
        JSONObject parameters = new JSONObject();
        try {
            parameters.put("date", date.getText().toString());
            parameters.put("time", time.getText().toString());
            parameters.put("approxAge", approxAge.getText().toString());
            parameters.put("approxHeight", approxHeight.getText().toString());
            parameters.put("identificationMarkOne", visibleIdentificationMarkOne.getText().toString());
            parameters.put("identificationMarkTwo", visibleIdentificationMarkTwo.getText().toString());
            parameters.put("otherDetails", otherDetails.getText().toString());
            parameters.put("heightUnit", heightUnit.getSelectedItem().toString());
            parameters.put("numberOfPics", 1);
            parameters.put("imageEncodedString", encodedImageString);
            parameters.put("name", name.getText().toString());
            parameters.put("lastLocatedAt", lastLocatedAt.getText().toString());
            parameters.put("gender", gender.getSelectedItem().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Map<String, String> headers = new HashMap<>();
        headers.put("User-Token", sessionManager.getUserToken());

        GSONRequest<ReportFoundResponse> reportFound = new GSONRequest<ReportFoundResponse>(Request.Method.POST, getResources().getString(R.string.report_found), ReportFoundResponse.class, headers, new Response.Listener<ReportFoundResponse>() {
            @Override
            public void onResponse(ReportFoundResponse response) {
                System.out.println("do we have anything?" + response.toString());
                if (response.isError()) {
                    dialog.dismiss();
                    Toast.makeText(ReportFoundActivity.this, "Unable to Report", Toast.LENGTH_SHORT).show();

                } else {
                    dialog.dismiss();
                    Toast.makeText(ReportFoundActivity.this, "Reported Successfully", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(ReportFoundActivity.this, BaseActivity.class));

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Log.d("error - reportFound", error.toString());
            }
        }, parameters);
        VolleyRequestQueue.getInstance(ReportFoundActivity.this).addToRequestQueue(reportFound);
    }

    private void initializeViews() {
        uploadPics = (Button) findViewById(R.id.button_upload_photos);
        submit = (Button) findViewById(R.id.submit_found);
        date = (Button) findViewById(R.id.set_date_button);
        time = (Button) findViewById(R.id.set_time_button);
        approxAge = (EditText) findViewById(R.id.approx_age_value);
        approxHeight = (EditText) findViewById(R.id.approx_height_value);
        visibleIdentificationMarkOne = (EditText) findViewById(R.id.point_one_value);
        visibleIdentificationMarkTwo = (EditText) findViewById(R.id.point_two_value);
        otherDetails = (EditText) findViewById(R.id.other_details_value);
        heightUnit = (Spinner) findViewById(R.id.height_unit);
        name = (EditText) findViewById(R.id.name_value);
        gender = (Spinner) findViewById(R.id.gender_value);
        uploadedImage = (ImageView) findViewById(R.id.uploaded_image);
        lastLocatedAt = (EditText) findViewById(R.id.last_located_at_value);
    }

    private void setDate() {
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog(v);
            }
        });
    }

    private void setTime() {
        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePickerDialog(v);
            }
        });
    }

    public void showTimePickerDialog(View v) {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getSupportFragmentManager(), "timePicker");
    }

    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }
}
