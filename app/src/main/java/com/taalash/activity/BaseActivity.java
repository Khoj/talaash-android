package com.taalash.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.taalash.R;
import com.taalash.util.SessionManager;

public class BaseActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    protected FrameLayout frameLayout;
    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_page);
        sessionManager = new SessionManager(getApplicationContext());
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        frameLayout = (FrameLayout) findViewById(R.id.content_frame);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        switch (id) {
            case R.id.home:
                setTitle("Talaash");
                startActivity(new Intent(this, HomeActivity.class));
                break;
            case R.id.my_case:
                setTitle("MyCase");
                startActivity(new Intent(this, MyCaseActivity.class));
                break;
            case R.id.create_a_case:
                setTitle("Create a Case");
                startActivity(new Intent(this, ReportMissingActivity.class));
                break;
            case R.id.report_found:
                setTitle("Report Found");
                startActivity(new Intent(this, ReportFoundActivity.class));
                break;
            case R.id.missing_reports:
                setTitle("News");
                startActivity(new Intent(this, NewsActivity.class));
                break;
            case R.id.matches:
                setTitle("Probable Matches");
                startActivity(new Intent(this, ProbableMatchesActivity.class));
                break;
            case R.id.settings:
                setTitle("Settings");
                startActivity(new Intent(this, SettingsActivity.class));
                break;
            case R.id.my_profile:
                setTitle("My Profile");
                startActivity(new Intent(this, MyProfileActivity.class));
                break;
            case R.id.logout:
                sessionManager.logoutUser();
                finish();
                break;
            default:

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
