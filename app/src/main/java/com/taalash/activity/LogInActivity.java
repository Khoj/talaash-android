package com.taalash.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.taalash.R;
import com.taalash.util.GSONRequest;
import com.taalash.util.SessionManager;
import com.taalash.util.VolleyRequestQueue;
import com.taalash.vo.LogInResponse;

import org.json.JSONException;
import org.json.JSONObject;

public class LogInActivity extends AppCompatActivity implements
        GoogleApiClient.OnConnectionFailedListener {

    private LoginButton loginButton;
    private CallbackManager callbackManager;
    private GoogleApiClient mGoogleApiClient;
    private TextView signUpWithTalaash;
    private SignInButton signInButton;
    private EditText userName, password;
    private Button signIn;
    private SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        session = new SessionManager(getApplicationContext());
        initializeViews();

        facebookLogin();
        googlePlusSign();
        signInWithTalaash();
        signUp();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    private void initializeViews() {
        loginButton = (LoginButton) findViewById(R.id.login_button);
        signInButton = (SignInButton) findViewById(R.id.sign_in_button);
        userName = (EditText) findViewById(R.id.userNameEditText);
        password = (EditText) findViewById(R.id.passwordEditText);
        signUpWithTalaash = (TextView) findViewById(R.id.sign_up_with_talaash);
        signIn = (Button) findViewById(R.id.login_with_talaash_button);
    }

    private void signInWithTalaash() {
        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject parameters = new JSONObject();
                try {
                    parameters.put("userName", userName.getText().toString());
                    parameters.put("password", password.getText().toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                GSONRequest<LogInResponse> loginRequest = new GSONRequest<>(Request.Method.POST, getResources().getString(R.string.login_user), LogInResponse.class, null, new Response.Listener<LogInResponse>() {
                    @Override
                    public void onResponse(LogInResponse response) {
                        System.out.println(response.toString());
                        if (response.isError()) {
                            Toast.makeText(LogInActivity.this, "Wrong LogIn Credentials", Toast.LENGTH_SHORT).show();
                        } else {
                            session.createLoginSession(response.getUserToken());
                            System.out.println(session.getUserToken());
                            startActivity(new Intent(LogInActivity.this, BaseActivity.class));
                            finish();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("error", error.toString());
                        Toast.makeText(LogInActivity.this, "Unable to reach our servers", Toast.LENGTH_SHORT).show();

                    }
                }, parameters);
                VolleyRequestQueue.getInstance(LogInActivity.this).addToRequestQueue(loginRequest);

            }
        });
    }

    private void signUp() {
        signUpWithTalaash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LogInActivity.this, SignUpActivity.class));
                finish();
            }
        });
    }

    private void googlePlusSign() {

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        signInButton.setSize(SignInButton.SIZE_STANDARD);
        signInButton.setScopes(gso.getScopeArray());
    }

    private void facebookLogin() {

        callbackManager = CallbackManager.Factory.create();

        loginButton.setReadPermissions("email");

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException e) {

            }
        });
    }


}
