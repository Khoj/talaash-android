package com.taalash.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.NetworkImageView;
import com.taalash.R;
import com.taalash.adapter.MyCaseAdapter;
import com.taalash.model.Status;
import com.taalash.util.GSONRequest;
import com.taalash.util.SessionManager;
import com.taalash.util.VolleyRequestQueue;
import com.taalash.vo.CaseResponse;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CaseViewActivity extends AppCompatActivity {

    private TextView myCaseIdNumber;
    private TextView name;
    private TextView height;
    private TextView identificationMarkOne;
    private TextView identificationMarkTwo;
    private RecyclerView statusList;
    private NetworkImageView image;
    private RelativeLayout container;
    private String caseId;
    private MyCaseAdapter adapter;
    private List<Status> statuses;
    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_case_view);
        sessionManager = new SessionManager(getApplicationContext());
        initializeViews();

        Bundle bundle = getIntent().getExtras();
        caseId = bundle.get("caseId").toString();

        statusList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        statusList.setLayoutManager(llm);
        populateViews();
    }

    private void initializeViews() {
        myCaseIdNumber = (TextView) findViewById(R.id.my_case_id_number);
        name = (TextView) findViewById(R.id.name);
        height = (TextView) findViewById(R.id.height);
        identificationMarkOne = (TextView) findViewById(R.id.identification_mark_one);
        identificationMarkTwo = (TextView) findViewById(R.id.identification_mark_two);
        statusList = (RecyclerView) findViewById(R.id.status_list);
        image = (NetworkImageView) findViewById(R.id.image);
        container = (RelativeLayout) findViewById(R.id.my_case_container);
    }

    private void populateViews() {
        Map<String, String> headers = new HashMap<>();
        headers.put("User-Token", sessionManager.getUserToken());
        GSONRequest<CaseResponse> getCase = new GSONRequest<>(Request.Method.GET, getResources().getString(R.string.get_case_by_id).concat(caseId), CaseResponse.class, headers, new Response.Listener<CaseResponse>() {
            @Override
            public void onResponse(CaseResponse response) {
                container.setVisibility(View.VISIBLE);
                System.out.println(response.toString());
                myCaseIdNumber.setText(response.getCaseId());
                name.setText(response.getName().concat(",").concat(response.getAge().concat(",").concat(response.getSex())));
                height.setText(response.getHeight() + " " + response.getHeightUnit());
                identificationMarkOne.setText(response.getIdentificationMarkOne());
                identificationMarkTwo.setText(response.getIdentificationMarkTwo());
                statuses = response.getStatuses();
                adapter = new MyCaseAdapter(statuses);
                statusList.setAdapter(adapter);
                image.setImageUrl(response.getImagePaths().get(0), VolleyRequestQueue.getInstance(CaseViewActivity.this).getImageLoader());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                container.setVisibility(View.GONE);
                Toast.makeText(CaseViewActivity.this, "Unable to fetch the case", Toast.LENGTH_LONG).show();
                Log.d("error:myCase", error.toString());
            }
        });
        VolleyRequestQueue.getInstance(CaseViewActivity.this).addToRequestQueue(getCase);

    }


}
