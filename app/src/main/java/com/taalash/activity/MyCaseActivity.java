package com.taalash.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.NetworkImageView;
import com.taalash.R;
import com.taalash.adapter.MyCaseAdapter;
import com.taalash.model.Status;
import com.taalash.util.GSONRequest;
import com.taalash.util.SessionManager;
import com.taalash.util.VolleyRequestQueue;
import com.taalash.vo.CaseResponse;
import com.taalash.vo.CloseCaseResponse;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyCaseActivity extends BaseActivity {

    private TextView myCaseIdNumber, name, height, identificationMarkOne, identificationMarkTwo;
    private RecyclerView statusList;
    private MyCaseAdapter adapter;
    private List<Status> statuses;
    private Button close;
    private SessionManager sessionManager;
    private NetworkImageView image;
    private RelativeLayout container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.frame_my_case, frameLayout);
        initializeViews();
        sessionManager = new SessionManager(getApplicationContext());
        statusList.setHasFixedSize(true);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        statusList.setLayoutManager(llm);

        closeCase();
        populateViews();
    }

    private void closeCase() {
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Map<String, String> headers = new HashMap<>();
                headers.put("User-Token", sessionManager.getUserToken());
                GSONRequest<CloseCaseResponse> closeCase = new GSONRequest<>(Request.Method.POST, getResources().getString(R.string.close_case), CloseCaseResponse.class, headers, new Response.Listener<CloseCaseResponse>() {
                    @Override
                    public void onResponse(CloseCaseResponse response) {
                        if (response.isError()) {
                            Toast.makeText(MyCaseActivity.this, "Unable to close case at the moment", Toast.LENGTH_SHORT).show();
                        } else {
                            createCloseCaseAlertBox();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("error:closeCase", error.toString());
                        Toast.makeText(MyCaseActivity.this, "Unable to reach our servers", Toast.LENGTH_SHORT).show();
                    }
                }
                );
                VolleyRequestQueue.getInstance(MyCaseActivity.this).addToRequestQueue(closeCase);
            }
        });
    }

    private void createCloseCaseAlertBox() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MyCaseActivity.this);
        builder.setMessage("Your case has been closed. ")
                .setTitle("Case Closed")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(MyCaseActivity.this, BaseActivity.class));
                    }
                });


        AlertDialog dialog = builder.create();
        dialog.show();

    }

    private void populateViews() {
        Map<String, String> headers = new HashMap<>();
        headers.put("User-Token", sessionManager.getUserToken());
        GSONRequest<CaseResponse> getMyCase = new GSONRequest<>(Request.Method.POST, getResources().getString(R.string.my_case), CaseResponse.class, headers, new Response.Listener<CaseResponse>() {
            @Override
            public void onResponse(CaseResponse response) {
                container.setVisibility(View.VISIBLE);
                System.out.println(response.toString());
                myCaseIdNumber.setText(response.getCaseId());
                name.setText(response.getName().concat(",").concat(response.getAge().concat(",").concat(response.getSex())));
                height.setText(response.getHeight() + " " + response.getHeightUnit());
                identificationMarkOne.setText(response.getIdentificationMarkOne());
                identificationMarkTwo.setText(response.getIdentificationMarkTwo());
                statuses = response.getStatuses();
                adapter = new MyCaseAdapter(statuses);
                statusList.setAdapter(adapter);
                image.setImageUrl(response.getImagePaths().get(0), VolleyRequestQueue.getInstance(MyCaseActivity.this).getImageLoader());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                container.setVisibility(View.GONE);
                Toast.makeText(MyCaseActivity.this, "No Case Exists or Unable to fetch your case", Toast.LENGTH_LONG).show();
                Log.d("error:myCase", error.toString());
            }
        });
        VolleyRequestQueue.getInstance(MyCaseActivity.this).addToRequestQueue(getMyCase);

    }

    private void initializeViews() {
        myCaseIdNumber = (TextView) findViewById(R.id.my_case_id_number);
        name = (TextView) findViewById(R.id.name);
        height = (TextView) findViewById(R.id.height);
        identificationMarkOne = (TextView) findViewById(R.id.identification_mark_one);
        identificationMarkTwo = (TextView) findViewById(R.id.identification_mark_two);
        statusList = (RecyclerView) findViewById(R.id.status_list);
        close = (Button) findViewById(R.id.close_case);
        image = (NetworkImageView) findViewById(R.id.image);
        container = (RelativeLayout) findViewById(R.id.my_case_container);
        container.setVisibility(View.GONE);
    }

}
