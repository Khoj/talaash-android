package com.taalash.activity;

import android.os.Bundle;

import com.taalash.R;

/**
 * Created by imittal on 5/20/16.
 */
public class ProbableMatchesActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.frame_probable_matches, frameLayout);

    }
}
