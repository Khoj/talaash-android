package com.taalash.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.taalash.R;
import com.taalash.util.GSONRequest;
import com.taalash.util.SessionManager;
import com.taalash.util.VolleyRequestQueue;
import com.taalash.vo.GenerateOtpResponse;
import com.taalash.vo.SignUpResponse;

import org.json.JSONException;
import org.json.JSONObject;

public class SignUpActivity extends AppCompatActivity {

    private EditText name, phoneNumber, enterOtp, password, confirmPassword;
    private Button signUp, generateOtp;
    private RelativeLayout otpLayout;
    private String otp;
    private SessionManager session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        session = new SessionManager(getApplicationContext());
        initializeViews();
        callGenerateOtpService();
        callVerifyOtpService();
    }

    private void callVerifyOtpService() {
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otpLayout.setVisibility(View.GONE);
                JSONObject parameters = new JSONObject();
                try {
                    parameters.put("userName", name.getText().toString());
                    parameters.put("password", password.getText().toString());
                    parameters.put("phoneNumber", phoneNumber.getText().toString());
                    parameters.put("correctOtp", otp);
                    parameters.put("enteredOtp", enterOtp.getText().toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                GSONRequest<SignUpResponse> verifyOtp = new GSONRequest(Request.Method.POST, getResources().getString(R.string.verify_otp), SignUpResponse.class, null, new Response.Listener<SignUpResponse>() {

                    @Override
                    public void onResponse(SignUpResponse response) {
                        if (response.getError()) {
                            Toast.makeText(SignUpActivity.this, "OTP Verification Failed", Toast.LENGTH_SHORT).show();

                        } else {
                            session.createLoginSession(response.getUserToken());
                            System.out.println(session.getUserToken());
                            startActivity(new Intent(SignUpActivity.this, BaseActivity.class));
                            finish();

                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("error - verifyOtp", error.toString());
                        Toast.makeText(SignUpActivity.this, "Unable to reach our servers", Toast.LENGTH_SHORT).show();
                    }
                }, parameters);
                VolleyRequestQueue.getInstance(SignUpActivity.this).addToRequestQueue(verifyOtp);
            }
        });
    }

    private void callGenerateOtpService() {
        generateOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (password.getText().toString().equals(confirmPassword.getText().toString()) && phoneNumber.getText().toString().length() == 10) {
                    JSONObject parameters = new JSONObject();
                    try {
                        parameters.put("phoneNumber", phoneNumber.getText().toString());
                        parameters.put("userName", name.getText().toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    GSONRequest<GenerateOtpResponse> generateOtp = new GSONRequest(Request.Method.POST, getResources().getString(R.string.generate_otp), GenerateOtpResponse.class, null, new Response.Listener<GenerateOtpResponse>() {

                        @Override
                        public void onResponse(GenerateOtpResponse response) {
                            System.out.println(response.toString());
                            if (response.isError()) {
                                if (response.getOtp().equals("Please Choose another user name")) {
                                    Toast.makeText(SignUpActivity.this, "Please Choose another user name", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(SignUpActivity.this, "Unable to reach our servers", Toast.LENGTH_SHORT).show();
                                }
                            }
                            otpLayout.setVisibility(View.VISIBLE);
                            otp = response.getOtp();

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(SignUpActivity.this, "Unable to reach our servers", Toast.LENGTH_SHORT).show();
                            Log.d("error - generateOtp", error.toString());
                        }
                    }, parameters);
                    VolleyRequestQueue.getInstance(SignUpActivity.this).addToRequestQueue(generateOtp);
                } else {
                    Toast.makeText(SignUpActivity.this, "Passwords don't match", Toast.LENGTH_SHORT).show();

                }
            }

        });
    }

    private void initializeViews() {
        name = (EditText) findViewById(R.id.name_value);
        phoneNumber = (EditText) findViewById(R.id.phone_value);
        enterOtp = (EditText) findViewById(R.id.otp_value);
        signUp = (Button) findViewById(R.id.sign_up);
        generateOtp = (Button) findViewById(R.id.generate_otp);
        password = (EditText) findViewById(R.id.password_value);
        otpLayout = (RelativeLayout) findViewById(R.id.enter_otp_layout);
        otpLayout.setVisibility(View.GONE);
        confirmPassword = (EditText) findViewById(R.id.confirm_password_value);
    }

}
