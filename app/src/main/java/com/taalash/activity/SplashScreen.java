package com.taalash.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.taalash.R;
import com.taalash.util.SessionManager;

public class SplashScreen extends AppCompatActivity implements Animation.AnimationListener {

    public SessionManager sessionManager;
    private Animation fadeIn;
    private ImageView logo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        logo = (ImageView) findViewById(R.id.logo);
        sessionManager = new SessionManager(getApplicationContext());
        fadeIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.smash_screen_anim);
        fadeIn.setAnimationListener(this);
        logo.startAnimation(fadeIn);


    }


    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        redirectToLoginScreenOrHomePage();
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    private void redirectToLoginScreenOrHomePage() {
        System.out.println("splash" + sessionManager.getUserToken());
        if (sessionManager.getUserToken() == "" || sessionManager.getUserToken() == null) {
            startActivity(new Intent(this, LogInActivity.class));
            finish();
        } else {
            startActivity(new Intent(this, BaseActivity.class));
            finish();
        }
    }
}
